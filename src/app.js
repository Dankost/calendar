import React from 'react'
import { render } from 'react-dom'
import {
    BrowserRouter as Router,
    Route,
    Link
  } from 'react-router-dom';
import {MyApp} from './calendar';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import  InfoShow  from './dayTask';
import InfoSave from './form';
export class App extends React.PureComponent {
    render() {
        return (
            <Provider store={store} >
                <Router>
                    <div>
                        <Route exact path='/' component={MyApp} />
                        <Route path="/:dayId/:taskId" component={InfoSave} />
                        <Route exact path="/:dayId" component={InfoShow}/>
                    </div>
                </Router>
            </Provider>
        )
    }
}