import React from 'react';
import ReactDOM from 'react-dom';
import { store } from './redux/store';
import { getTaskList, removeTask } from './redux/actions';
import { Provider, connect } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

const mapStateToProps = (store) => {
    return {
        task: store.task,
        load: store.isRequest
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTaskList: async (id) => {
            await getTaskList(id, dispatch);
        },
        deleteTask: async (id, taskid) => {
            await removeTask(id, taskid, dispatch);
        }
    }
}
class InfoShow extends React.PureComponent {
    async componentWillMount() {
        await this.props.getTaskList(this.props.match.params.dayId);
    }
    deleteBack = (id, taskid) => {
        this.props.deleteTask(id, taskid);
    }
    render() {
        const dayId = this.props.match.params.dayId;
        const tasks = this.props.task;
        if (this.props.load) {
            return <div>Loading...</div>
        }
        else if (!tasks || tasks && tasks.length < 1) {
            return <ul>
                <li><Link to="/">HOME</Link></li>
                <li><Link to={`/${dayId}/add`}>ADD NEW</Link></li>
            </ul>
        }
        else if (!this.props.load) return (
            <div>
                <ul>{tasks.map((task, index) => <li key={index}>{task.task}<Link to={`/${dayId}/${task._id}`}>редактировать</Link><button onClick={() => this.deleteBack(dayId, task._id)}> x</button></li>)}</ul>
                <ul>
                    <li><Link to="/">HOME</Link></li>
                    <li><Link to={`/${dayId}/add`}>ADD NEW</Link></li>
                </ul>
            </div>

        );
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InfoShow)
