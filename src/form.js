import React from 'react';
import ReactDOM from 'react-dom';
import { store } from './redux/store';
import { addTask } from './redux/actions';
import { Provider, connect } from 'react-redux';
class InfoSave extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            task: "",
            add: true
        }
    }
    onChange = (e) => {
        const name = e.target.name;
        this.setState({ [name]: e.target.value });
    }
    sendTask = async () => {
        const tasks = {
            task: this.state.task,
        }
        if (tasks.task.length > 0) {
            await this.props.addTask(this.props.match.params.dayId, tasks, this.props.match.params.taskId, this.state.add);
            this.removeTask();
            this.props.history.push(`/${this.props.match.params.dayId}`);
        }
        else
            alert("fill input");
    }
    removeTask = () => {
        this.setState({
            task: "",
        })
    }
    componentWillMount() {
        const tasks = this.props.task;
        const task = tasks.find(el => el._id == this.props.match.params.taskId);
        if (this.props.match.params.taskId == "add") {
            this.setState({
                task: ""
            })
        }
        else {
            this.setState({
                task: task.task,
                add: false
            })
        }
    }
    render() {
        return (
            <div className="inputWrap">
                <input name="task" onChange={this.onChange} value={this.state.task}></input>
                <button className="save" onClick={this.sendTask}>Save</button>
                <button className="remove" onClick={this.removeTask}>remove</button>
            </div>
        );
    }

}

const mapStateToProps = (store) => {
    return {
        task: store.task
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addTask: async (id, task, taskId, add) => {
            await addTask(id, task, dispatch, taskId, add);
        }
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InfoSave)
