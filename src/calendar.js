import React, { Component } from 'react';
import Calendar from 'react-calendar';
export class MyApp extends Component {
  state = {
    date: new Date(),
  }

  onChange = date => this.setState({ date })
  getDay = event => {
    let curr_date = event.getDate();
    let curr_month = event.getMonth() + 1;
    let curr_year = event.getFullYear();

    let fullDate = curr_date + "" + curr_month + "" + curr_year;

    console.log(fullDate);
    this.props.history.push(fullDate);
  };
  render() {
    console.log(this.props);
    return (
      <div>
        <Calendar
          onClickDay={this.getDay}
          onChange={this.onChange}
          value={this.state.date}
        />
      </div>
    );
  }
}