import firebase from 'firebase'
export const config = {
    apiKey: "AIzaSyA65tPnbuEykgiGyk0xkTlthWF3eJXE-ik",
    authDomain: "todo-ad3b4.firebaseapp.com",
    databaseURL: "https://todo-ad3b4.firebaseio.com",
    projectId: "todo-ad3b4",
    storageBucket: "todo-ad3b4.appspot.com",
    messagingSenderId: "609235650326"
  };
firebase.initializeApp(config);
export default firebase;