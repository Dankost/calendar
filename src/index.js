import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './app';
import {InfoShow} from './dayTask';

ReactDOM.render(<App />, document.getElementById('root'));
