export const actionType = {
    addTask: "ADDTASK",
    removeTask: "REMOVETASK",
    showTask: "SHOW",
    request: "REQUEST",
    success: "SUCCESS",
    fail: "FAIL"
}