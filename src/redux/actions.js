import { actionType } from './constants';
import firebase from '../firebase.js';

export const getTaskList = async (id, dispatch) => {
    const itemsRef = firebase.database().ref('Tasks' + id);
    dispatch(getRequest());
    const item = await itemsRef.once("value");
    dispatch(successRequest());
    let result = item.val() || [];
    for (const key in result) {
        result[key]._id = key
    }
    dispatch(showTask(result));
}
export const showTask = (result) => ({
    type: actionType.showTask,
    Task: result
})
export const addTask = async (id, tasks, dispatch, taskId, add) => {
    if (add) {
        const itemsRef = firebase.database().ref('Tasks' + id);
        dispatch(getRequest());
        const result = await itemsRef.push(tasks);
        dispatch(successRequest());
        console.log(result);
        return;
    }
    else {
        const itemsRef = firebase.database().ref('Tasks' + id +'/' + taskId);
        dispatch(getRequest());
        const result = await itemsRef.set({
            "task": tasks.task
        });
        dispatch(successRequest());
        console.log(result);
        return;
    }
}
export const removeTask = async (id, taskId, dispatch) => {
    const itemsRef = firebase.database().ref('Tasks' + id + '/' + taskId);
    dispatch(getRequest());
    await itemsRef.remove();
    const result = itemsRef;
    console.log(result);
    dispatch(successRequest());
    await getTaskList(id, dispatch);
}
export const getRequest = () => ({
    type: actionType.request
})
export const successRequest = () => ({
    type: actionType.success
})
export const failRequest = (error) => ({
    textError: error,
    type: actionType.fail
})