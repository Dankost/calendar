import { actionType } from './constants';

const initialState = {
    task: [],
    isRequest: false,
    error: " "
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.addTask:
            return {
                task: [...state.task, action.taskList]
            }
        case actionType.showTask: 
            return {
                task: Object.values(action.Task)
            }
        case actionType.request:
            return {
                isRequest: true
            }
        case actionType.success: 
            return {
                isRequest: false
            }
        case actionType.fail: 
            return {
                error: action.taskError
            }
        default:
            return state
    }
}